# Present-Day Pearls #

Classic pearls get reinvented with a contemporary twist that’s fresh and young. Pearls are a traditional way to execute a polished look, but mixing them with modern elements makes them more versatile and relevant than ever before. These updated pearl designs belong in every woman’s fashion jewelry collection and are a perfect choice for the next generation. Olga Prieto’s white pearl earrings ($120) have an unexpected hinged design that allows the white pearls to swing back and forth for extra movement. The Wasabi pearl and leather bracelet ($107) and necklace ($157) each contrast the rustic texture of brown leather with strands of luminous freshwater pearls. The Wendy Mink Mother-of-Pearl necklace ($122) has a flirty design that’s dripping with enticing elongated white pearls suspended from 18k gold-plated chains. Catherine Weitzman’s pearl cluster ring ($170) and earrings ($160) are a creative way to display dozens of loose keishi pearls all at once.

# The New Classics #

### Stackables ###

Piling on an armful of bangles or wearing multiple stacking rings lets you build a look that’s completely unique to you. Achieve a balanced look with the mixed metal Belargo six stone stacking ring set ($125) that can be layered together for three times the fun. This set of Andara cubic zirconia wavy bangles ($125) will make a big impact when the three bracelets are perfectly nestled together. Or, choose a new ring or bangle from Max & Chloe to stack with the other pieces in your [fashion or fine jewelry](https://www.blufashion.com/) collection.


### Drop-Hoop Earrings ###

Unlike the traditional hoop design, the drop hoop hangs lower and creates a wider range of movement for a more free-spirited feel. These Blu Bijoux crystal drop-hoop earrings ($40) have a versatile design that’s crafted of gold plate with a dusting of white Austrian crystals. The Ron Hami three-row hoop earrings ($3,920) are effortlessly chic with three 18k yellow gold concentric circles hanging from a diamond pave ear post.

### Twisted Glamour ###

Simple chains become fashion-forward when they are interwoven with sparkling elements. This Ceek white twisted pearl bracelet ($100) pops with pearlized beads and white rhinestones amidst a jumble of yellow-gold plated chains. The messy strands come together nicely to fasten with an oversized lobster-claw clasp. For an unexpected yet polished approach, try this ABS diamond and pearl twisted necklace ($225) that’s crafted with contrasting materials for a stunning effect.